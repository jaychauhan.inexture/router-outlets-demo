import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagegreenComponent } from './pagegreen.component';

describe('PagegreenComponent', () => {
  let component: PagegreenComponent;
  let fixture: ComponentFixture<PagegreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PagegreenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PagegreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
