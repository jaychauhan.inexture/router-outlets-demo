import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageredComponent } from './pagered.component';

describe('PageredComponent', () => {
  let component: PageredComponent;
  let fixture: ComponentFixture<PageredComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageredComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
