import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageblueComponent } from './pageblue/pageblue.component';
import { PagegreenComponent } from './pagegreen/pagegreen.component';
import { PageredComponent } from './pagered/pagered.component';
import { SidebarComponent } from './sidebar/sidebar.component';

const routes: Routes = [
  {
    path: "chat",
    component: SidebarComponent,
    outlet: "sidebar"
  },

  {
    path: 'page-red',
    component: PageredComponent,
    outlet: 'outlet-a'
  },
  {
    path: 'page-green',
    component: PagegreenComponent,
    outlet: 'outlet-b'
  },
  {
    path: 'page-green',
    component: PagegreenComponent,
    outlet: 'outlet-c'
  },
  {
    path: 'page-blue',
    component: PageblueComponent,
    outlet: 'outlet-b'
  },
  {
    path: 'page-blue',
    component: PageblueComponent,
    outlet: 'outlet-c'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
