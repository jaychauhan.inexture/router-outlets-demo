import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageblueComponent } from './pageblue.component';

describe('PageblueComponent', () => {
  let component: PageblueComponent;
  let fixture: ComponentFixture<PageblueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageblueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageblueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
