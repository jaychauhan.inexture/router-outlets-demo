import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { PageredComponent } from './pagered/pagered.component';
import { PagegreenComponent } from './pagegreen/pagegreen.component';
import { PageblueComponent } from './pageblue/pageblue.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    PageredComponent,
    PagegreenComponent,
    PageblueComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
